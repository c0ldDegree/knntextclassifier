﻿using Iveonik.Stemmers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KnnTextClassifier
{
    public class KNNClassifier
    {
        string dataDirPath;
        private double[] IDFVector;
        public List<string> results;
        public KNNClassifier(string _dataDirPath = @"C:\Users\HP\Documents\VisualStudio2017\Projects\knntextclassifier\KnnTextClassifier\dataset")
        {
            dataDirPath = _dataDirPath;
            results = new List<string>();
        }
        public List<string> categoriesList => GetDirectoriesName();
        public Tuple<List<string>, Dictionary<string, int>> dictionary => EnumerateWords();
        private List<string> GetDirectoriesName()
        {
            List<string> directories = new List<string>();
            foreach (var d in Directory.GetDirectories(dataDirPath))
            {
                directories.Add(new DirectoryInfo(d).Name);
            }
            return directories;
        }
        private int FilesCount()
        {
            int count = 0;
            foreach (var d in Directory.GetDirectories(dataDirPath))
            {
                count += Directory.GetFiles(d).Count();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Lista słow, słownik słowo->indeks z listy słow, lista kategorii-kategoria pod tym samym indeksem co dokument w macierzy, macierz wag słów</returns>
        public Tuple<List<string>, Dictionary<string, int>, List<string>, double[][]> Teach()
        {
            var p = this.dictionary;
            var words = p.Item1;
            var dictionary = p.Item2;
            var categories = this.categoriesList;
            double[][] wordsM = new double[FilesCount()][];
            IDFVector = new double[words.Count];
            List<string> documentsCategory = new List<string>();
            int k = 0;
            foreach (var c in categories)
            {
                foreach (string f in Directory.GetFiles(dataDirPath + @"\" + c))//przechodzimy po wszystkich dokumentach i obliczamy liczbe wystapien słow w kategorii
                {
                    double[] wordsCount = new double[words.Count];
                    int count = WordsInText(File.ReadAllText(f), dictionary, ref wordsCount);
                    for (int j = 0; j < wordsCount.Length; j++)
                        wordsCount[j] /= count;//TF
                    wordsM[k] = wordsCount;
                    documentsCategory.Add(c);
                    k++;
                }
            }
            for (int i = 0; i < words.Count; i++)//obliczenie macierzy
            {
                int wordFreq = 0;//Liczba dokumentów ze słowem words[i]
                for (int j = 0; j < wordsM.Length; j++)
                {
                    if (wordsM[j][i] > 0)
                        wordFreq++;
                }
                for (int j = 0; j < wordsM.Length; j++)
                {
                    IDFVector[i] = Math.Log(documentsCategory.Count / wordFreq);
                    wordsM[j][i] *= Math.Log(documentsCategory.Count / wordFreq);//TF*IDF
                }
            }
            return new Tuple<List<string>, Dictionary<string, int>, List<string>, double[][]>(words, dictionary, documentsCategory, wordsM);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentPath">ścieżka do pliku analizowanego</param>
        /// <param name="k">liczba sąsiadów</param>
        /// <param name="teachResult">wynik metody teach</param>
        /// <returns></returns>
        public string PredictClass(string documentPath, int k, Tuple<List<string>, Dictionary<string, int>, List<string>, double[][]> teachResult)
        {
            double[] documentVector = new double[teachResult.Item1.Count];
            int count = WordsInText(File.ReadAllText(documentPath), teachResult.Item2, ref documentVector);
            for (int j = 0; j < documentVector.Length; j++)
            {
                documentVector[j] /= count;//TF
                documentVector[j] *= IDFVector[j];
            }
            double[] distances = new double[teachResult.Item4.Length];
            for (int i = 0; i < teachResult.Item4.Length; i++)
            {
                for (int j = 0; j < teachResult.Item4[i].Length; j++)
                {
                    distances[i] += (documentVector[j] - teachResult.Item4[i][j]) * (documentVector[j] - teachResult.Item4[i][j]);
                }
                distances[i] = Math.Sqrt(distances[i]);
            }

            List<Tuple<double, string>> result = new List<Tuple<double, string>>();
            for (int i = 0; i < distances.Length; i++)
                result.Add(new Tuple<double, string>(distances[i], teachResult.Item3[i]));
            result.Sort((x, y) => x.Item1.CompareTo(y.Item1));
            var result2 = result.Take(k);
            var tempClass = result2.GroupBy(x => x.Item2).Select(g => new { ClassName = g.Key, Count = g.Count() }).OrderByDescending(x => x.Count);
            foreach (var el in tempClass)
                results.Add(el.ClassName + "---" + el.Count.ToString());
                //Console.WriteLine(el.ClassName + "---" + el.Count.ToString());
            return tempClass.First().ClassName;
        }
        /// <summary>
        /// Dla wszystkich plików z folderu zwraca słownik słów
        /// </summary>
        /// <returns>Zwraca listę unikalnych słów oraz słownik słowo->indeks na liście</returns>
        private Tuple<List<string>, Dictionary<string, int>> EnumerateWords()
        {
            var stemmer = new EnglishStemmer();
            List<string> words = new List<string>();
            Dictionary<string, int> wordsDict = new Dictionary<string, int>();
            foreach (var f in DirSearch(dataDirPath))
            {
                string text = File.ReadAllText(f, Encoding.UTF8);
                var matches = Regex.Matches(text, @"\b[^\d\W]+\b|\b\d+\b");// @"\b[^\d\W]+\b"

                foreach (Match match in matches)
                {
                    string word = stemmer.Stem(match.Value.ToLower());
                    if (!wordsDict.ContainsKey(word))
                    {
                        words.Add(word);
                        wordsDict.Add(word, words.Count - 1);
                    }
                }
            }
            return new Tuple<List<string>, Dictionary<string, int>>(words, wordsDict);
        }
        private int WordsInText(string text, Dictionary<string, int> dictionary, ref double[] wordsCount)
        {
            var stemmer = new EnglishStemmer();
            int count = 0;
            var matches = Regex.Matches(text, @"\b[^\d\W]+\b|\b\d+\b");
            foreach (Match match in matches)
            {
                string word = stemmer.Stem(match.Value.ToLower());
                try
                {
                    dictionary.TryGetValue(word, out int v);
                    wordsCount[v]++;
                    count++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return count;
        }

        private List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    files.AddRange(DirSearch(d));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return files;
        }
    }
}
