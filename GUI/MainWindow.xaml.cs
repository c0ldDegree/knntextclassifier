﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string dataPath;
        public string textPath;
        public int k;

        public MainWindow()
        {
            InitializeComponent();
            k = 3;
        }

        private void DataButton_Click(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.Description = "Wybierz katalog z plikami treningowmi";
                dlg.ShowNewFolderButton = true;
                DialogResult result = dlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    dataPath = dlg.SelectedPath;
                    DataBox.Text = dlg.SelectedPath;
                    if (textPath != null)
                        StartButton.IsEnabled = true;
                }
            }
        }

        private void TextButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = System.AppDomain.CurrentDomain.BaseDirectory,
                Title = "Wybierz tekst do klasyfikacji"
            };
            if (openFile.ShowDialog() == true)
            {
                textPath = openFile.FileName;
                TextBox.Text = openFile.FileName;
                if (dataPath != null)
                    StartButton.IsEnabled = true;
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            ResultText.Text = "Czekaj, trwają obliczenia...";
            KnnTextClassifier.KNNClassifier classifier = new KnnTextClassifier.KNNClassifier(dataPath);
            var model = classifier.Teach();
            string className = classifier.PredictClass(textPath, k, model);
            ResultText.Text = className;
            string categ = "";
            foreach (var val in classifier.results)
                categ += "[" + val + "], ";
            ListText.Text = categ;
        }

        private void NeighboursBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Int32.TryParse(NeighboursBox.Text, out int newK) && (newK > 0))
                k = newK;
            else
                NeighboursBox.Text = k.ToString();
        }
    }
}
